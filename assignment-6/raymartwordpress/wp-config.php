<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bsit' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{fZBk-PaQmALY:4T)c5oVr&%/Hw`/hO-siSdJ1Gz&Lxj(}mV<%BPXkmv8HikU- K' );
define( 'SECURE_AUTH_KEY',  '[Ew{g5nRATd=cdVYKH`d%S=xP^HUi%TUu5uu)Fy55:Uy sZQ6a8/72kUpfR%wzJ?' );
define( 'LOGGED_IN_KEY',    'P6+0<Az[#HZLv0_X4{)S)VZeE%n%b7| z+LO!{f9M,t+}{M6|o n@|MJZ.pg@@;x' );
define( 'NONCE_KEY',        '[$XG.)ewzqSY!!SuMU_RDZ5LGhe7N[x}u3XlnA ~5.a4R]k hGi|D?;abx-=q#`y' );
define( 'AUTH_SALT',        'R3X1X]pV#A~u,uAvA4mz[D oP H1}yB|FX)h#_JN1^F!^MJ|dyHXC_Rt7<lN1]=U' );
define( 'SECURE_AUTH_SALT', 'f3BFbh.6xVndWm<3CeTAgC_6<Ap(FHna7!.bIjxhL2JQ4{pON.YWe S{T@*XB^cG' );
define( 'LOGGED_IN_SALT',   'SS^9yXEzKnA+ZX52#!m$(/zV?=8%9.z)rLTC0s-9uM1SqM~#kM?R&d5o85_8roZZ' );
define( 'NONCE_SALT',       'Z9V.QVQ/K$pR?z|^cD~l4Lv !a**&d&;oeXpzmeoLJaYW3f Oh1s-C7p))YUv,=k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
