<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Upload</title>
</head>
<body>


	<form enctype="multipart/form-data" method="POST">
		<input type="file" name="image" id="media">
	</form>

	<script>
	function upload(input){
		var xhr = new XMLHttpRequest();
		xhr.upload.onprogress = function(e) {
			console.log(e.loaded, e.total)
		}
		xhr.upload.onload = function(e) {
			console.log('file upload')
		}
		xhr.open("POST", "/test/uploadfile.php", true);
		xhr.send(new FormData(input.parentElement));
	}
	</script>
</body>
</html>