    var calculator = angular.module("calculator", []);
    calculator.controller('calcontrol', function($scope) {
    $scope.numbers = "0";
    $scope.counter = 0;
    $scope.answer = 0;
    
    $scope.Input = function(number) {
        var tmp = true;
        if($scope.answer != 0) {
            $scope.answer = 0;
            $scope.numbers = "0";
            tmp = true;
        }
            if(angular.equals('+', number) || 
                angular.equals('-', number) ||
                angular.equals('*', number) || 
                angular.equals('/', number)) {
                var count = "+|-|*|/".indexOf($scope.numbers.charAt($scope.numbers.length - 1));

                $scope.counter = $scope.numbers.length + 1;
            } 

                else {
                tmp = true;
                if($scope.numbers.substring($scope.counter).length < 10) {
                    if(angular.equals(number, ".")) {
                        var k = 0;
                        for(var j = 0; j < $scope.numbers.substring($scope.counter).length; j++){
                            if(angular.equals(".", $scope.numbers.substring($scope.counter).charAt(j))) {
                                k = k + 1;
                            }
                        }
                }

                        else {
                            tmp = true;
                }
            }
                
        }
                    return tmp;
    }
    
    $scope.operate = function(op) {
        if($scope.Input(op)) {
            $scope.numbers = $scope.numbers + op;
        }
    }
    
    
    $scope.press = function(number) {
        if($scope.Input(number)) {
            if(angular.equals(number, 'clear')){
                $scope.numbers = $scope.numbers.slice(0 , $scope.numbers.length - 1);    
            } else {
                if (angular.equals($scope.numbers, "0")) {
                    $scope.numbers = "";
                    $scope.numbers += number;

                } else if (angular.equals(".", $scope.numbers)){

                    $scope.numbers = "0.";
                    $scope.numbers += number;
                } else {

                    $scope.numbers += number;
                }
            }
        } else {

            if(angular.equals(number, 'clear')){
                $scope.numbers = $scope.numbers.slice(0 , $scope.numbers.length - 1);    
            }
        }
    }
    
    $scope.equal = function() {
        var calResult = "+|-|*|/".indexOf($scope.numbers.charAt($scope.numbers.length - 1));
       
        if(eval($scope.numbers) == 0){
            $scope.numbers = "0";
        } else {
            $scope.answer = eval($scope.numbers);
            $scope.numbers = $scope.answer;
        }
    }
});
